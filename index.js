const express = require('express');
const cors = require('cors');
const mysql = require('mysql');

const app = express();

const SELECT_ALL_PRODUCTS_QUERY = 'SELECT * FROM portfolio';

const connection = mysql.createConnection({
	host: 'localhost',
	user: 'root',
	password: 'root',
	database: 'g_gills',
	port: 8889
});

connection.connect(err => {
	if(err) {
		return err;
	} else {
		console.log('Connected to MySQL Server');
	}
});

// console.log(connection);


app.use(cors());

app.get('/', (req, res) => {
	res.send('go to /portfolio to see portfolio');
});

// add item
app.get('/portfolio/add', (req, res) => {
	const { name, content } = req.query;
	const INSERT_PRODUCT_QUERY = `INSERT INTO portfolio (name, content) VALUES('${name}','${content}')`;
	connection.query(INSERT_PRODUCT_QUERY, (err, results) => {
		if(err) {
			return res.send(err);

		} else {
			res.send('successfully added portfolio item');

		}
	});
});

// update item
app.get('/portfolio/update', (req, res) => {
	const { name, content, id } = req.query;
	const UPDATE_PORTFOLIO_QUERY = `UPDATE portfolio SET name='${name}', content='${content}' WHERE id = ${id}`;

	connection.query(UPDATE_PORTFOLIO_QUERY, (err, results) => {
		if(err) {
			console.log(err);
			return res.send(err);
		} else {
			res.send('Item succesfully updated');
		}
	});
});

// delete item
app.get('/portfolio/delete', (req, res) => {
	const { id } = req.query;
	const DELETE_PORTFOLIO_QUERY = `DELETE FROM portfolio WHERE id IN (${id})`;

	connection.query(DELETE_PORTFOLIO_QUERY, (err, results) => {
		if(err) {
			return res.send(err);
		} else {
			res.send('Item deleted successfully');
		}
	});
	// console.log(req.query);
});

// get all items
app.get('/portfolio', (req, res) => {
	connection.query(SELECT_ALL_PRODUCTS_QUERY, (err, results) => {
		if(err) {
			return res.send(err)
		} 
		else {
			return res.json({
				data: results
			})
		}
	});
});

app.listen(4000, () => {
	console.log(`Products server listening on port 4000`);
});
